import mpp.EX1Q8;

public class MppRunner {

    public static void main(String[] args) {
        final String[] testArgs = {"2", "4", "8", "16", "32"};
        for (String numOfThreads : testArgs) {
            System.out.format("Running %s with %s threads\n", EX1Q8.class, numOfThreads);
            String[] testArg = {numOfThreads};
            EX1Q8.main(testArg);
            System.out.format("Finished Running %s with %s threads\n", EX1Q8.class, numOfThreads);
        }
    }
}
