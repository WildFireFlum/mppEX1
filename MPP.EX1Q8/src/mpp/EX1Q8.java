package mpp;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class EX1Q8 {

    final static int expectedNumOfArgs = 1;

    public static void main(String[] args) {
        if (args.length != expectedNumOfArgs) {
            System.out.format("usage: EX1Q8 [num of threads]");
            return;
        }

        final int numOfThreads = Integer.parseInt(args[0]);

        EX1Q8 context = new EX1Q8();
        final long startTime = System.currentTimeMillis();
        AtomicInteger sharedCount = new AtomicInteger(0);
        List<Thread> threadList = new LinkedList<Thread>();

        // Spawn threads
        for (int i = 0; i < numOfThreads; i++) {
            String threadName = String.format("Thread %d", i);
            CountThread currentCountThread = context.new CountThread(threadName, sharedCount);
            threadList.add(currentCountThread.getThread());
            currentCountThread.start();
        }

        // Wait for all
        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.format("Something bad happened: %s\n", e.getMessage());
            }
        }
        final long endTime = System.currentTimeMillis();

        System.out.format("sharedCount: %d\n", sharedCount.get());
        System.out.format("Time elapsed: %d milliseconds\n", endTime - startTime);
    }

    private class CountThread implements Runnable {
        static final private int numOfIterations = 1000000;

        private Thread thread;
        private String threadName;
        private AtomicInteger sharedCount;

        CountThread(final String name, AtomicInteger sharedCount) {
            System.out.format("Creating %s\n", name);
            threadName = name;
            this.sharedCount = sharedCount;
            thread = new Thread(this, threadName);
        }

        public Thread getThread() {
            return thread;
        }

        public void run() {
            System.out.format("Running %s, Going to run %d times\n", threadName, numOfIterations);

            for (int i = 0; i < numOfIterations; i++) {
                sharedCount.getAndIncrement();
            }
            System.out.format("Thread %s exiting.\n", threadName);
        }

        public void start() {
            System.out.format("Starting %s\n", threadName);
            thread.start();
        }
    }


}
