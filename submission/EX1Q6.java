package mpp;

import java.util.LinkedList;
import java.util.List;

public class EX1Q6 {

    final static int expectedNumOfArgs = 1;

    public static void main(String[] args) {
        if (args.length != expectedNumOfArgs) {
            System.out.format("usage: mpp.MppRunner [num of threads]");
            return;
        }

        final int numOfThreads = Integer.parseInt(args[0]);

        EX1Q6 context = new EX1Q6();
        final long startTime = System.currentTimeMillis();
        MySharedInteger sharedCount = context.new MySharedInteger(0);
        List<Thread> threadList = new LinkedList<Thread>();

        // Spawn threads
        for (int i = 0; i < numOfThreads; i++) {
            String threadName = String.format("Thread %d", i);
            CountThread currentCountThread = context.new CountThread(threadName, sharedCount);
            threadList.add(currentCountThread.getThread());
            currentCountThread.start();
        }

        // Wait for all
        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.format("Something bad happened: %s\n", e.getMessage());
            }
        }
        final long endTime = System.currentTimeMillis();

        System.out.format("sharedCount: %d\n", sharedCount.getNum());
        System.out.format("Time elapsed: %d milliseconds\n", endTime - startTime);
    }

    private class CountThread implements Runnable {
        static final private int numOfIterations = 1000000;

        private Thread thread;
        private String threadName;
        private MySharedInteger sharedCount;

        CountThread(final String name, MySharedInteger sharedCount) {
            System.out.format("Creating %s\n", name);
            threadName = name;
            this.sharedCount = sharedCount;
            thread = new Thread(this, threadName);
        }

        public Thread getThread() {
            return thread;
        }

        public void run() {
            System.out.format("Running %s, Going to run %d times\n", threadName, numOfIterations);

            int localCount;
            for (int i = 0; i < numOfIterations; i++) {
                localCount = sharedCount.getNum();
                localCount++;
                sharedCount.setNum(localCount);
            }
            System.out.format("Thread %s exiting.\n", threadName);
        }

        public void start() {
            System.out.format("Starting %s\n", threadName);
            thread.start();
        }
    }

    private class MySharedInteger {
        private volatile int num;

        public MySharedInteger(int num) {
            this.num = num;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }

}
