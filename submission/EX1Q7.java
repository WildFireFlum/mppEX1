package mpp;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class EX1Q7 {

    final static int expectedNumOfArgs = 1;

    public static void main(String[] args) {
        if (args.length != expectedNumOfArgs) {
            System.out.format("usage: EX1Q7 [num of threads]");
            return;
        }

        final int numOfThreads = Integer.parseInt(args[0]);

        EX1Q7 context = new EX1Q7();
        final long startTime = System.currentTimeMillis();
        MySharedCount sharedCount = context.new MySharedCount();
        List<Thread> threadList = new LinkedList<Thread>();

        // Spawn threads
        for (int i = 0; i < numOfThreads; i++) {
            String threadName = String.format("Thread %d", i);
            CountThread currentCountThread = context.new CountThread(threadName, sharedCount);
            threadList.add(currentCountThread.getThread());
            currentCountThread.start();
        }

        // Wait for all
        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                System.out.format("Something bad happened: %s\n", e.getMessage());
            }
        }
        final long endTime = System.currentTimeMillis();

        System.out.format("sharedCount: %d\n", sharedCount.getNum());
        System.out.format("Time elapsed: %d milliseconds\n", endTime - startTime);
    }

    private class CountThread implements Runnable {
        static final private int numOfIterations = 1000000;

        private Thread thread;
        private String threadName;
        private MySharedCount sharedCount;

        CountThread(final String name, MySharedCount sharedCount) {
            System.out.format("Creating %s\n", name);
            threadName = name;
            this.sharedCount = sharedCount;
            thread = new Thread(this, threadName);
        }

        public Thread getThread() {
            return thread;
        }

        public void run() {
            System.out.format("Running %s, Going to run %d times\n", threadName, numOfIterations);

            for (int i = 0; i < numOfIterations; i++) {
                sharedCount.inc();
            }
            System.out.format("Thread %s exiting.\n", threadName);
        }

        public void start() {
            System.out.format("Starting %s\n", threadName);
            thread.start();
        }
    }

    /**
     *  A shared integer which does not allow concurrent writes
     *
     *  @note: Allows reads to occur when the value is written
     */
    private class MySharedCount {
        private volatile int num;
        private volatile ReentrantLock lock;

        public MySharedCount() {
            this.num = 0;
            this.lock = new ReentrantLock();
        }

        public int getNum() {
            return num;
        }

        /**
         * Sets the value of num
         * Prevents multiple simultaneous writes
         */
        public void inc() {
            lock.lock();
            this.num++;
            lock.unlock();
        }
    }

}
