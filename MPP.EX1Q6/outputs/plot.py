
#cd E:\Temp

import plotly
from plotly.graph_objs import Scatter, Layout

xs = [112, 316, 560, 1042, 1928]
ys = [2, 4, 8, 16, 32]

myCount = Scatter(
    x=xs,
    y=ys,
    mode='lines+markers',
    name='MySharedCounter',
)

trace1 = Scatter(
    x=[1, 2, 3, 4],
    y=[16, 5, 11, 9]
)
data = Data([myCount, trace1])

plotly.offline.plot({
    "data": data,
    "layout": Layout(title="Shared Counter Multicore Performance")
})