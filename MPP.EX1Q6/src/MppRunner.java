import mpp.EX1Q6;

public class MppRunner {

    public static void main(String[] args) {
        final String[] testArgs = {"2", "4", "8", "16", "32"};
        for (String numOfThreads : testArgs) {
            System.out.format("Running %s with %s threads\n", mpp.EX1Q6.class, numOfThreads);
            String[] testArg = {numOfThreads};
            EX1Q6.main(testArg);
            System.out.format("Finished Running %s with %s threads\n", mpp.EX1Q6.class, numOfThreads);
        }
    }
}
